var actions = require('./actions.json');
var builder = require('./builder.js');
var simulator = require('./simulator.js');

var combatDuration = 300; // seconds

console.log(actions);

var nbIndiv = 20;

var indivs = [];

// Initializes the first generation
builder.initGeneration(indivs, nbIndiv, actions.actions);

console.error("Individuals: ");
console.log(indivs);

// Measure the fitness of all individuals
var fitnesses = simulator.computeFitnesses(indivs, combatDuration)


console.log(fitnesses);