
// Create random action list for first generation
var buildRandomActionList = function(actions){

	// first get the number of actions in the list
	var actionNumber = Math.floor((Math.random() * (actions.length * 2)) + 1);

	var actionList = [];

	for (var i = actionNumber - 1; i >= 0; i--) {
		actionList[i] = actions[Math.floor((Math.random() * actions.length))]
	}

	return actionList;
}


var initGeneration = function (indivs, nbIndiv, actions){
	for (var i = nbIndiv - 1; i >= 0; i--) {
		indivs[i] = {
			action: buildRandomActionList(actions)
		};
	}
}

// exports

module.exports.initGeneration = initGeneration;