var buffs = require('./buffs.json').buffs;

var passiveBuffs = [];
var buffDico = [];

for (var i = 0; i < buffs.length; i++){
    var buff = buffs[i];
    if (buff.duration === "passive") {
        passiveBuffs.push(buff);
    }
    buffDico[buff.name] = buff;
}

// Character stats
var spellPower = 36898;
var baseCrit = 12401;
var baseMastery = 3733;
var baseHaste = 3651;
var baseVers = 3178;

// Stat ratings 
var critRating = 400;
var mastRating = 533;
var hasteRating = 375;
var versRating = 475;

var critPer = baseCrit / critRating + 20;
var mastPer = baseMastery / mastRating + 6;
var hastePer = baseHaste / hasteRating;
var versPer = baseVers / versRating;


var computeCritMultiplier = function (action, curBuffs) {
    var mult = 2;

    for (var i = 0; i < passiveBuffs.length; i++) {
        var buff = passiveBuffs[i];
        if (buff.effect.target === "CRIT_DMG_MULT") {
            mult *= buff.effect.multValue;
        }
    }

    return mult;

}

var applyCritBuffs = function () {

    var addPer = 20;
    var addStat = baseCrit;

    for (var i = 0; i < passiveBuffs.length; i++) {
        var buff = passiveBuffs[i];
        if (buff.effect.target === "STATS") {
            for (var j = 0; j < buff.effect.statIncrease.length; j++) {
                var statInc = buff.effect.statIncrease[j];
                if (statInc.stat === "CRIT") {
                    if (statInc.increaseType === "PERCENTAGE") {
                        addPer += statInc.increaseValue;
                    } else if (statInc.increaseType === "VALUE") {
                        addStat += statInc.increaseValue;
                    }
                }
            }
        }
    }

    critPer = addStat / critRating + addPer;
}


var computeCritChance = function (action, curBuffs) {
    applyCritBuffs();
    return critPer;
}

var isCrit = function (action, curBuffs) {
    var isCrit = false;
    var roll = Math.random();
    if (roll < computeCritChance(action, curBuffs)) {
        isCrit = true;
    }
    return isCrit;
}

var computeDamage = function (action, curBuffs) {

    var dmg = 0;
    // base damage
    dmg += action.damage.base + action.damage.spellpower * spellPower;

    // crit ? 
    if (isCrit(action, curBuffs)) {
        dmg *= computeCritMultiplier(action, curBuffs);
    }

    return dmg;

}



// Compute fitness for one single individual
var computeFitness = function (individual, combatDuration) {

    var cooldowns = [];
    var curBuffs = [];
    var time = 0;
    var actionFlow = [];
    var damage = 0;

    var idleTime = 0;

    while (time < combatDuration) {

        var action = {};

        for (var i = individual.action.length - 1; i >= 0; i--) {
            if (cooldowns[individual.action[i].name] && cooldowns[individual.action[i].name] > 0) {

                //console.log(cooldowns[individual.action[i].name])
                continue;
            } else {
                action = individual.action[i];
                break;
            }
        }

        if (!action.casttime) {
            // action is idle
            action = {
                name: "idle",
                casttime: 0.1,
                damage: null
            };
            idleTime += action.casttime;
        }
        // reduce cooldowns
        for (var spell in cooldowns) {
            cooldowns[spell] = cooldowns[spell] - action.casttime;
        }
        // reduce buffs
        for (var buff in curBuffs) {
            curBuffs[buff] = curBuffs[buff] - action.casttime;
        }

        actionFlow.push(action);
        time += action.casttime;
        //console.log("Action: ");
        //console.log(action);
        if (action.damage != null) {

            var dmg = computeDamage(action, curBuffs);
            //console.log("Damage Applied: " + dmg);
            damage += dmg;
        }
        cooldowns[action.name] = action.cooldown;

        // applyBuffs
        if (action.buff != null) {
            curBuffs[action.buff] = buffDico[action.buff];
        }
    }

    var fitness = damage / time;

    console.log("-------------------------------------------------------");
    console.log("For individual:");
    console.log(individual);
    // console.log("Actions:");
    // console.log(actionFlow);

    // console.log("Damage:");
    // console.log(damage);

    console.log("Fitness:");
    console.log(fitness);

    console.log("Idle Time:");
    console.log(idleTime);

    return fitness;

}

// Compute fitneses for all individuals of generation
var computeFitnesses = function (indivs, combatDuration) {
    var fitnesses = [];
    for (var i = indivs.length - 1; i >= 0; i--) {
        fitnesses[i] = {
            fitness: computeFitness(indivs[i], combatDuration),
            individual: indivs[i]
        };
    }
    return fitnesses;
}



module.exports.computeFitnesses = computeFitnesses;

